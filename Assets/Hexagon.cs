﻿using UnityEngine;
using System.Collections;

public class Hexagon
{
    public Vector2 OffsetCoord;
    public Vector2 AxialCoord;
    public Vector3 CubeCoord;
    public Vector3 WorldCoord;
    public int TextureID;
    public bool Occupied;
    public GameObject OnThisSpace; //TODO: Concider Renaming

    public Hexagon(Vector2 offsetCoord, Vector3 worldCoord)
    {
        int X = (int)(offsetCoord.x - (offsetCoord.y + (offsetCoord.y % 2)) / 2);
        int Z = (int)offsetCoord.y;
        int Y = -X - Z;

        WorldCoord = worldCoord;
        OffsetCoord = offsetCoord;
        CubeCoord = new Vector3(X, Y, Z);
        AxialCoord = new Vector2(X, Z);
    }

    public override string ToString()
    {
        return "Cube: " + CubeCoord.ToString() + " Axial: " + AxialCoord.ToString() + " Offset: " + OffsetCoord.ToString();
    }
}
