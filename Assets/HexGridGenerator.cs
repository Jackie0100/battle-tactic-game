﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HexGridGenerator : MonoBehaviour 
{
    public int row = 2;
    public int column = 10;
    public int Radius;
    public int Height;
    public float RowHeight;
    public float HalfWidth;
    public float Width;
    public float ExtraHeight;
    public float Edge;
    public GeneratorOptions GenerationOption;

    [SerializeField]
    Texture TextureSheet;
    [SerializeField]
    int SheetRow = 4;
    [SerializeField]
    int SheetColumn = 4;

    List<Vector3> Vertices = new List<Vector3>();
    List<Vector3> Normals = new List<Vector3>();
    List<Vector2> Uvs = new List<Vector2>();
    List<int> Triangles = new List<int>();

    List<Hexagon> Hexagons = new List<Hexagon>();

    [SerializeField]
    Transform DEBUGOBJ;

    void Awake()
    {
        Radius = 2;
        Height = 2 * Radius;
        RowHeight = 1.5f * Radius;
        HalfWidth = Mathf.Sqrt((Radius * Radius) - ((Radius / 2) * (Radius / 2)));
        Width = 2 * HalfWidth;
        ExtraHeight = Height - RowHeight;
        Edge = RowHeight - ExtraHeight;
    }

	// Use this for initialization
	void Start ()
    {
        for (int i = 0; i < row; i++) //Y/Z
        {
            int j = (GenerationOption == GeneratorOptions.Additional && i % 2 == 1 ? 1 : 0);
            while (j < column) //X
            {
                if (GenerationOption == GeneratorOptions.Removed && j == column - 1 && i % 2 == 0)
                {
                    j++;
                    continue;
                }
                GenerateGrid(Vertices, Uvs, Triangles, j, i);
                j++;
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = Vertices.ToArray();
        mesh.uv = Uvs.ToArray();
        mesh.triangles = Triangles.ToArray();
        
        //mesh.normals = Normals.ToArray();
        MeshFilter meshfilter = gameObject.AddComponent<MeshFilter>();
        //add a mesh renderer to the GO the script is attached to
        gameObject.AddComponent<MeshRenderer>();
        meshfilter.mesh = mesh;
        GetComponent<Renderer>().material.mainTexture = TextureSheet;
        gameObject.AddComponent<MeshCollider>().sharedMesh = mesh;

	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    void GenerateGrid(List<Vector3> vertices, List<Vector2> uv, List<int> tris, int x, int y)
    {
        int index = vertices.Count;
        float uvx = Random.Range(1, 5);
        float uvy = Random.Range(1, 5);
        float uvlenght = 1.0f / SheetRow;
        float uvheight = 1.0f / SheetColumn;
        float offset = 0.0001f;
        //top
        vertices.Add(new Vector3(0 + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, -Radius + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(0.5f / (float)SheetRow + (uvlenght * uvx), 1 / (float)SheetColumn + (uvheight * uvy)));
        //topright
        vertices.Add(new Vector3(HalfWidth + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, -Radius / 2 + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(1 / (float)SheetRow + (uvlenght * uvx), 0.75f / (float)SheetColumn + (uvheight * uvy)));
        //bottomright
        vertices.Add(new Vector3(HalfWidth + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, Radius / 2 + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(1 / (float)SheetRow + (uvlenght * uvx), 0.25f / (float)SheetColumn + (uvheight * uvy)));
        //bottom
        vertices.Add(new Vector3(0 + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, Radius + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(0.5f / (float)SheetRow + (uvlenght * uvx), 0 + (uvheight * uvy)));
        //bottomleft
        vertices.Add(new Vector3(-HalfWidth + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, Radius / 2 + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(0 + (uvlenght * uvx), 0.25f / (float)SheetColumn + (uvheight * uvy)));
        //topleft
        vertices.Add(new Vector3(-HalfWidth + (HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0, -Radius / 2 + ((Radius / 2) * 3 * y)));
        uv.Add(new Vector2(0 + (uvlenght * uvx), 0.75f / (float)SheetColumn + (uvheight * uvy)));

        tris.AddRange(new int[]
        {
            1 + index, 0 + index, 5 + index, 2 + index, 4 + index, 3 + index, 2 + index, 1 + index, 4 + index, 1 + index, 5 + index, 4 + index
        });

        Hexagon hex = new Hexagon(new Vector2(x, y), new Vector3((HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), 0f, ((Radius / 2) * 3 * y)));
        Hexagons.Add(hex);


        ////TODO: DEBUG STUFF
        //Transform go = GameObject.Instantiate(DEBUGOBJ);
        //go.name = hex.ToString();
        //go.transform.position = new Vector3((HalfWidth * 2 * x) - ((HalfWidth) * (y % 2)), -0.4f, ((Radius / 2) * 3 * y));
    }

    //public Vector3 WorldToHexagon(Vector3 position)
    //{
    //    /*return new Vector3(Mathf.Round((Mathf.Sqrt(3.0f) / 3.0f * position.x - position.z / 3.0f) / (float)Radius),
    //        Mathf.Round(-(Mathf.Sqrt(3.0f) / 3.0f * position.x + position.z / 3.0f) / (float)Radius),
    //        Mathf.Round((2.0f / 3.0f) * (position.z / (float)Radius)));*/

    //    /*float temp = Mathf.Floor(position.x + Mathf.Sqrt(3) * position.z + 1);
    //    float q = Mathf.Floor((Mathf.Floor(2 * position.x + 1) + temp) / 3) / 2;
    //    float r = Mathf.Floor((temp + Mathf.Floor(-position.x + Mathf.Sqrt(3) * position.z + 1)) / 3) / 2;
    //    return new Vector3(q, 0, r);*/

    //    /*return Hexagons.First(h => 
    //        h.AxialCoord.x == Mathf.Round((Mathf.Sqrt(3.0f) / 3.0f * position.x - position.z / 3.0f) / (float)Radius) &&
    //        h.AxialCoord.y == Mathf.Round((2.0f / 3.0f) * (position.z / (float)Radius)));*/

    //    Vector3 temp = new Vector3();
    //    Vector3 loc = new Vector3(Mathf.Round(position.x / Width), this.transform.position.y, Mathf.Round(position.z / RowHeight));
    //    for (int i = -1; i <= 1; i++)
    //    {
    //        for (int j = -1; j <= 1; j++)
    //        {
    //            if (Vector3.Distance(temp, position) > Vector3.Distance(new Vector3((loc.x - i) * Width, loc.y, (loc.z - j) * RowHeight), temp))
    //            {
    //                temp = new Vector3((loc.x - i), loc.y, (loc.z - j));
    //            }
    //        }
    //    }
    //    return temp;
    //}

    public Hexagon WorldToHexagon(Vector3 position)
    {
        Hexagon retval = null;
        Vector3 temp = new Vector3();
        foreach (Hexagon hex in Hexagons)
        {
            if (Vector3.Distance(position, temp) > Vector3.Distance(position, hex.WorldCoord))
            {
                temp = hex.WorldCoord;
                retval = hex;
            }
        }
        return retval;
    }

    public enum GeneratorOptions
    {
        None, Additional, Removed,
    }
}